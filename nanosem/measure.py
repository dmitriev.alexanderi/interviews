#!/usr/bin/env python3
import asyncio
from aiohttp import ClientSession
from time import monotonic
from uuid import uuid4


async def fetch(sem, url, session):
    async with sem:
        async with session.post(url, json={"text": str(uuid4())}) as response:
            await response.read()


async def run(r):
    url = "http://localhost:8080/revert"
    tasks = []
    sem = asyncio.Semaphore(500)

    async with ClientSession() as session:
        for _ in range(r):
            task = asyncio.ensure_future(fetch(sem, url, session))
            tasks.append(task)

        responses = asyncio.gather(*tasks)
        await responses

number = 10000
loop = asyncio.get_event_loop()
future = asyncio.ensure_future(run(number))
start = monotonic()
loop.run_until_complete(future)
end = monotonic()
print(f"Total time: {end-start} seconds, per second: {number/(end-start)}")
