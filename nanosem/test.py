#!/usr/bin/env python3
import unittest
import nano
import json
from tornado.testing import AsyncHTTPTestCase
from fibonacci import Fibonacci, FiboCache

class TestNano(AsyncHTTPTestCase):
    def get_app(self):
        return nano.NanoApp()

    def revert_helper(self, sendable, expected):
        response = self.fetch('/revert', method="POST",
                              body=json.dumps({"text": sendable}))
        self.assertEqual(response.code, 201)
        dic = json.loads(response.body.decode('utf-8'))
        self.assertIn("response", dic)
        self.assertEqual(dic["response"], expected)

    def test_revert(self):
        cases=(("abcXYZ", "ZYXcba"),
               ("ы", "ы"),
               ("тест", "тсет"),
               ("000🕳xxx💣111", "111💣xxx🕳000"))
        for s, e in cases:
            self.revert_helper(s, e)

    def fibo_helper(self, start, end, expected):
        response = self.fetch('/fibonacci', method="POST",
                              body=json.dumps({"start":start, "end": end}))
        self.assertEqual(response.code, 200)
        dic = json.loads(response.body.decode('utf-8'))
        self.assertIn("response", dic)
        self.assertEqual(dic["response"], expected)
    
    def test_fibo(self):
        cases = ((1, 1, [0]),
                 (1, 2, [0, 1]),
                 (1, 3, [0, 1, 1]),
                 (6, 9, [5, 8, 13, 21]))
        for s, e, x in cases:
            self.fibo_helper(s, e, x)

class TestFibo(unittest.TestCase):
  def setUp(self):
    self.fib = Fibonacci(FiboCache())
    # http://oeis.org/A000045/list
    self.oeis = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377,
                 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657,
                 46368, 75025, 121393, 196418, 317811, 514229, 832040,
                 1346269,2178309,3524578,5702887,9227465, 14930352,
                 24157817, 39088169, 63245986, 102334155]
  
  def test_nth(self):
    for i in range(1,20):
     assert self.fib.calc_nth(i) == self.oeis[i - 1]
  
  def test_wrong(self):
    with self.assertRaises(Exception):
      self.fib.calc_range(100, 50)
    with self.assertRaises(Exception):
      self.fib.calc_range(None, None)
    with self.assertRaises(Exception):
      self.fib.calc_range(100, -500)
  
  def test_range(self):
    assert self.fib.calc_range(1, len(self.oeis)) == self.oeis
    for i in range(1, len(self.oeis)):
      assert self.fib.calc_range(i, i)[0] == self.oeis[i - 1]


if __name__ == '__main__':
    unittest.main()
