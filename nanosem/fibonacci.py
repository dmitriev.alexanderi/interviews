#!/usr/bin/env python3
from sys import getsizeof


class FiboCache(list):
    """
    Кэшик, который умеет огранивать свой размер. 
    Хранение ранее посчитанных значений в кэше позволяет очень сильно
    увеличить быстродействие, особенно на больших диапазонах.
    Не потокобезопасный.
    """
    def __init__(self, size_mb=256):
        super().__init__(self)
        self.extend((None, 0, 1))
        self.size = size_mb * 1024 * 1024
        self.full = False
    
    def append(self, item):
        if self.full:
            return
        if getsizeof(self) + getsizeof(item) > self.size:
            self.full = True
        else:
            super().append(item)


class Fibonacci:
    """
    Класс вычисляет числа Фибоначчи: как по номеру, так и целыми диапазонами.
    Номера при этом отсчитываются от 1.
    """
    def __init__(self, cache: FiboCache):
        self.cache = cache

    def __calc_next(self, v, pv):
        """
        Вычисляет следующее число Фибоначчи и пытается добавить его в кэш.
        """
        v, pv = v + pv, v
        self.cache.append(v)
        return v, pv

    def calc_nth(self, num: int):
        """
        Вычисляет num-тое число Фибоначчи
        """
        if num < 0:
            raise ValueError("Negative fibonacci numbers are not supperted")
        if num < len(self.cache):
            return self.cache[num]
        start, v, pv = len(self.cache) - 1, self.cache[-1], self.cache[-2]
        for i in range(start + 1, num + 1):
            v, pv = self.__calc_next(v, pv)
        return v

    def calc_range(self, start: int, end: int):
        """
        Вычисляет диапазон чисел Фибоначчи
        """
        if start <= 0 or end <= 0 or end < start:
            raise ValueError(f"Boudaries error ({start}, {end})")
        end += 1
        total = len(self.cache)
        # Если начало лежит в кэше, возьму оттуда хотя бы часть данных
        if start < total:
            subend = min(total, end)
            result = self.cache[start:subend]
            start = subend
        # Иначе нужно посчитать 1 или 2 значения
        else:
            result = []
            for i in range(2):
                result.append(self.calc_nth(start))
                start += 1
                if end <= start:
                    break
        
        if end <= start:
            return result

        v, pv = result[-1], result[-2]
        for i in range(start, end):
            v, pv = self.__calc_next(v, pv)
            result.append(v)
        return result


if __name__ == '__main__':
    fib = Fibonacci(FiboCache(10))
    print(fib.calc_range(1, 3))
    print(fib.calc_range(6, 9))
    print([fib.calc_nth(i) for i in range(1, 20)])
