#!/usr/bin/env python3
import tornado.web
import json
from tornado.options import define, options, parse_command_line
from fibonacci import Fibonacci, FiboCache
import logging, logging.config

# Пользовательские параметры
define("cache_size", default=256, help="cache size in Mb", type=int)
define("port", default=8080, help="run on the given port", type=int)

# Логгирование
logging.config.fileConfig("logging.conf")
logger = logging.getLogger("nano")


class RevertHandler(tornado.web.RequestHandler):
    async def post(self):
        try:
            received = json.loads(self.request.body)
            if "text" not in received:
                raise KeyError("No text provided")
            if not isinstance(received["text"], str):
                raise TypeError("'text' is not a string")
            self.set_status(201)
            await self.finish({"response": received["text"][::-1]})
        except Exception as e:
            logger.warning(str(e))
            self.set_status(400)


class FibonacciHandler(tornado.web.RequestHandler):
    async def post(self):
        try:
            received = json.loads(self.request.body)
            start = int(received["start"])
            end = int(received["end"])
            response = self.application.fib.calc_range(start, end)
            if not response:
                raise Exception()
            self.set_status(200)
            await self.finish({"response": response})
        except Exception as e:
            logger.warning(str(e))
            self.set_status(400)


class NanoApp(tornado.web.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(handlers=[
                            (r"/revert", RevertHandler),
                            (r"/fibonacci", FibonacciHandler),
                         ],
                         *args, **kwargs)
        self.fib = Fibonacci(FiboCache(options.cache_size))


if __name__ == "__main__":
    parse_command_line()
    app = NanoApp()
    app.listen(options.port)
    tornado.ioloop.IOLoop.current().start()
