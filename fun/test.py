import asyncio
import pytest
import fun
import json
from aiohttp import web

async def post_and_test(client, sendable, assert_text="OK"):
    resp = await client.post('/visited_links', json=sendable)
    assert resp.status == 200
    text = await resp.text()
    data = json.loads(text)
    assert assert_text == data["status"]
    return data

async def get_and_test(client, start, end, assert_text="OK"):
    resp = await client.get(f"/visited_domains?from={start}&to={end}")
    assert resp.status == 200
    text = await resp.text()
    data = json.loads(text)
    assert assert_text == data["status"]
    return data

async def test_all(aiohttp_client, loop):
    backend = fun.Backend(rargs={"address": "redis://localhost:9124"})
    hist = fun.History(backend)
    app = web.Application()
    app.on_startup.append(backend.redis_connection)
    app.add_routes([web.get('/visited_domains', hist.get_handler),
                    web.post('/visited_links', hist.post_handler)])
    client = await aiohttp_client(app)
    resp = await post_and_test (client, sendable={"links": ["a.ru", "b.com"]})
    ts1=int(resp["timestamp"])
    await asyncio.sleep(1)
    resp = await post_and_test (client, sendable={"links": ["a.ru", "c.org"]})
    ts2=int(resp["timestamp"])
    resp = await get_and_test(client, ts1, ts2)
    assert len(resp["links"]) == 3
    resp = await get_and_test(client, 0, 1)
    assert len(resp["links"]) == 0
    resp = await get_and_test(client, 0, 1)
    assert len(resp["links"]) == 0
    
    await post_and_test (client, sendable={"wrong": ["data.ru"]},
                         assert_text="Bad Request")
    await get_and_test (client, "wrong", "stamp", assert_text="Bad Request")