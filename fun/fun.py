#!/usr/bin/python3
# Как я храню данные.
# Оглавление со scores == таймстемп,
# Записи ключ == таймстемп, значение == массив
#
# index: ts1 "ts1" ts2 "ts2" ts4 "ts4"
# ts1: urls...
# ts2: urls...
# ts4: urls...

import aioredis
import json
import time
from aiohttp import web
from urllib.parse import urlparse

index = "INDEX"


def url_to_domain(url: str) -> str:
    """
    Функция вытаскивает домен из ссылки
    """
    parsed = urlparse(url)
    if not parsed.scheme:
        parsed = urlparse("//"+url)
    return parsed.netloc.lower()


class FunResponse(web.Response):
    """
    По условиям задачи ответ всегда выдается в виде джсон.
    Класс унифицирует возврат из хендлеров.
    """
    def __init__(self, status_class: web.HTTPException = web.HTTPOk(),
                 data: dict = {}):
        data["status"] = status_class.reason
        text = json.dumps(data, ensure_ascii=False)
        super().__init__(text=text)

class Backend:
    """
    Бэкенд общается с редисом, хранит и ищет данные.
    """
    def __init__(self, redis=None, rargs : dict = None):
        self.redis = redis
        self.rargs = rargs

    async def redis_connection(self, app):
        """
        Подключение к редису в момент инициализации приложения
        """
        if self.redis is None:
            self.redis = await aioredis.create_redis(**self.rargs)
    
    async def save(self, timestamp, urls):
        """
        Метод сохраняет данные для временной метки:
        1. Вытаскивает домены из урлов
        2. Делает запись в "оглавлении"
        3. Добавляет домены в соответствующий массив
        """ 
        domains = set(map(url_to_domain, urls))
        if not domains:
            raise Exception("No valid domains")
        key = str(timestamp)
        await self.redis.zadd(index, timestamp, key)
        await self.redis.sadd(key, *domains)
        print(timestamp) 

    async def find(self, start, end):
        """
        Метод ищет данные в БД.
        """
        data = set()
        ranges = await self.redis.zrangebyscore(index, min=start, max=end)
        for key in ranges:
            urls = await self.redis.smembers(key)
            for i in urls:
                data.add(i.decode("utf-8"))
        return data


class History:
    """
    Этот класс реагирует на запросы клиента.
    """
    def __init__(self, backend):
        self.backend = backend
    
    async def post_handler(self, request):
        """
        Хендлер для POST-запросов
        """
        try:
            raw = await request.json()
            timestamp = int(time.time())
            await self.backend.save(timestamp, raw["links"])
            return FunResponse(data={"timestamp": timestamp})
        except Exception as e:
            print(e)
            return FunResponse(web.HTTPBadRequest())
    
    async def get_handler(self, request):
        """
        Хендлер для GET-запросов
        """
        try:
            start = int(request.rel_url.query['from'])
            end = int(request.rel_url.query['to'])
            domains = await self.backend.find(start, end)
            return FunResponse(data={"links": [*domains]})
        except Exception as e:
            print(e)
            return FunResponse(web.HTTPBadRequest())


def init_func(argv):
    # для запуска через
    # python3 -m aiohttp.web -H localhost -P 8080 fun:init_func
    backend = Backend(rargs={"address": "redis://localhost"})
    hist = History(backend)
    app = web.Application()
    app.on_startup.append(backend.redis_connection)
    app.add_routes([web.get('/visited_domains', hist.get_handler),
                    web.post('/visited_links', hist.post_handler)])
    return app


if __name__ == "__main__":
    app = init_func(None)
    web.run_app(app, host="localhost", port=8080)
