#include <stdint.h> // uint64_t, uint32_t

/*
  чтение указанного количества байт физической памяти по заданному адресу в указанный буфер
  функция вернет количество прочианных байт (меньшее или 0 означает ошибку - выход за пределы памяти)
 */
typedef int (*PREAD_FUNC) (void *buf, const unsigned int size, const uint64_t physical_addr);

#define CHECK_BIT(value, bit) ((uint64_t)value & (0x1UL << bit))
#define NULL ((void*) 0)

/*
  move_bits:
  @src: исходное значение
  @high: верхняя граница диапазона
  @low: нижняя граница диапазона
  @new_low: новая нижняя граница или -1, чтобы не смещать.

  Вспомогательная функция для перемещения битов туда-сюда.
  Номера битов от 0 до 63, границы включены (как в руководстве Intel)

  Return: число со смещенными битами и 0 за пределами диапазона.
 */
uint64_t move_bits (uint64_t src, int high, int low, int new_low)
{
  uint64_t mask = 1ul << (high - low);
  mask <<= 1;
  mask--;
  mask <<= low;

  /* Синтаксический сахар. */
  if (new_low == -1)
    new_low = low;

  return (new_low - low < 0) ? (src & mask) >> (low - new_low) :
                               (src & mask) << (new_low - low);
}

/* Функция трансляции в режиме PAE. */
int va2pa_pae (const unsigned int linear_addr,
               const unsigned int cr3,
               const PREAD_FUNC   read_func,
               uint64_t          *phys_addr)
{
  uint64_t pdpte_addr;
  uint64_t pdpte_entry;
  uint64_t pde_addr;
  uint64_t pde_entry;
  uint64_t size;

  /* CR3 указывает на начало PDPTE. PDPTE - это 4 64-битных элемента. */
  pdpte_addr = move_bits (cr3,         31, 5, -1) |
               move_bits (linear_addr, 31, 30, 0);

  size = sizeof (pdpte_entry);
  if (size != read_func (&pdpte_entry, size, pdpte_addr))
    return -1;

  /* PDPTEi.P */
  if (!CHECK_BIT (pdpte_entry, 0))
    return -1;

  /* PDE присутствует всегда. */
  pde_addr = move_bits (pdpte_entry, 51, 12, -1) |
             move_bits (linear_addr, 29, 21, 3);

  size = sizeof (pde_entry);
  if (size != read_func (&pde_entry, size, cr3))
    return -1;

  /* PDE.P */
  if (!CHECK_BIT (pde_entry, 0))
    return -1;

  /* PDE.PS */
  if (CHECK_BIT (pde_entry, 7))
    {
      *phys_addr = move_bits (pde_entry,   51, 21, -1) |
                   move_bits (linear_addr, 20,  0, -1);
    }
  else
    {
      uint64_t pte_addr;
      uint64_t pte_entry;

      pte_addr = move_bits (pde_entry,   51, 21, -1) |
                 move_bits (linear_addr, 20, 12, 3);

      /* Читаю PTE */
      size = sizeof (pte_entry);
      if (size != read_func (&pte_entry, size, pte_addr))
        return -1;

      /* PDE.P */
      if (!CHECK_BIT (pte_entry, 0))
        return -1;
    }

  return 0;
}

/* Функция трансляциии адреса в 32-bit режиме. */
int va2pa_32 (const unsigned int linear_addr,
              const unsigned int cr3,
              const PREAD_FUNC   read_func,
              uint64_t          *phys_addr)
{
  uint64_t pde_addr = 0;
  uint32_t pde_entry = 0;
  int size;

  /* начало PDE хранится в CR3 */
  pde_addr = move_bits (cr3,         31, 12, -1) |
             move_bits (linear_addr, 31, 22, 2);

  /* Читаю PDE */
  size = sizeof (pde_entry);
  if (size != read_func (&pde_entry, size, pde_addr))
    return -1;

  /* PDE.P */
  if (!CHECK_BIT (pde_entry, 0))
    return -1;

  /* Здесь можно также проверить зарезервированные биты. Если они подняты,
     процессор вернет ошибку. */

  /* PDE.PS (bit 7) определяет размер страницы.
     Если поднят, то страницы по 4 Мб, PTE не используется. */
  if (CHECK_BIT (pde_entry, 7))
    {
      *phys_addr = move_bits (pde_entry,   20, 13, 32) |
                   move_bits (pde_entry,   31, 22, -1) |
                   move_bits (linear_addr, 21, 0,  -1);
    }
  else
    {
      uint64_t pte_addr = 0;
      uint32_t pte_entry = 0;

      pte_addr = move_bits (pde_entry,   31, 12, -1) |
                 move_bits (linear_addr, 21, 12, 2);

      /* Читаю PTE */
      size = sizeof (pte_entry);
      if (size != read_func (&pte_entry, size, pte_addr))
        return -1;

      /* PTE.P */
      if (!CHECK_BIT (pte_entry, 0))
        return -1;

      *phys_addr = move_bits (pte_entry,   31, 12, -1) |
                   move_bits (linear_addr, 11, 0,  -1);
    }

  return 0;
}

/* Собственно функция трансляции виртуального адреса в физический. */
int va2pa (const unsigned int virt_addr,
           const unsigned int level,
           const unsigned int root_addr,
           const PREAD_FUNC   read_func,
           uint64_t          *_phys_addr)
{
  int res = -1;
  uint64_t phys_addr;

  if (level == 2)
    res = va2pa_32 (virt_addr, root_addr, read_func, &phys_addr);
  else if (level == 3)
    res = va2pa_pae (virt_addr, root_addr, read_func, &phys_addr);
  else
    return -1;

  if (_phys_addr != NULL)
    *_phys_addr = phys_addr;

  return res;
}
